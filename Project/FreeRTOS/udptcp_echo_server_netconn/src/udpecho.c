/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */


#include "lwip/opt.h"
#include "stm322xg_eval_lcd.h"

#if LWIP_NETCONN

#include "lwip/api.h"
#include "lwip/sys.h"


#define UDPECHO_THREAD_PRIO  ( tskIDLE_PRIORITY + 3 )

static struct netconn *conn;
static struct netbuf *buf, *buf_res;
static struct ip_addr *addr;
static unsigned short port;
char text19[24] = "Response to 0x19 is 0x20", *data;
char text17[24] = "Response to 0x17 is 0x14";
u16_t len;
/*-----------------------------------------------------------------------------------*/
static void udpecho_thread(void *arg)
{
  err_t err;
  
  LWIP_UNUSED_ARG(arg);
	
	
  conn = netconn_new(NETCONN_UDP);
  if (conn!= NULL)
  {
    err = netconn_bind(conn, IP_ADDR_ANY, 11000);
    if (err == ERR_OK)
    {
      while (1) 
      {
        buf = netconn_recv(conn);
      
        if (buf!= NULL) 
        {
          addr = netbuf_fromaddr(buf);
          port = netbuf_fromport(buf);
					netbuf_data(buf, &data, &len);
					//if (port == 55056)
					{
						// Arkualni stav vozidla 
						if (data[0] == 0x01)
						{
							#define MESSAGE11   "Aktualni stav       "
							LCD_DisplayStringLine(LCD_LINE_5, (uint8_t*)MESSAGE11); 
							//netconn_connect(conn, addr, port);
						//buf->addr = NULL;
							//netconn_send(conn,buf_res);
						}
						
						// Hovor - Zadost vozidla
						if (data[0] == 0x19)
						{
							#define MESSAGE12   "Hovor Zadost vozidla"
							LCD_DisplayStringLine(LCD_LINE_5, (uint8_t*)MESSAGE12); 
							buf_res = netbuf_new();
							//data = netbuf_alloc(buf_res, sizeof(text));
							netbuf_ref( buf_res, text19, sizeof(text19));		
							
							
							netconn_connect(conn, addr, port);
						  //buf->addr = NULL;
							netconn_send(conn,buf_res);
							
							netbuf_delete(buf_res);			
							//netconn_close(buf);
							
						}
						// Textova Zprava
						if (data[0] == 0x17)
						{
							#define MESSAGE13   "Textova Zprava Odcho"
							LCD_DisplayStringLine(LCD_LINE_5, (uint8_t*)MESSAGE13);
							LCD_DisplayStringLine(LCD_LINE_6, (uint8_t*)data);
							buf_res = netbuf_new();
							netbuf_ref( buf_res, text17, sizeof(text17));		
							
							netconn_connect(conn, addr, port);
							netconn_send(conn,buf_res);
							
							netbuf_delete(buf_res);			
						}						
						netbuf_delete(buf);	
					}
									
        }
      }
    }
    else
    {
      printf("can not bind netconn");
    }
  }
  else
  {
    printf("can create new UDP netconn");
  }
}
/*-----------------------------------------------------------------------------------*/
void udpecho_init(void)
{
  sys_thread_new("udpecho_thread", udpecho_thread, NULL, DEFAULT_THREAD_STACKSIZE,UDPECHO_THREAD_PRIO );
}

/*-----------------------------------------------------------------------------------*/
void udpecho_commands(void)
{
  netbuf_data(buf, &data, &len);	
	
	switch (data[0])
	{
		case 0x01:
						netconn_connect(conn, addr, port);
						netconn_send(conn,buf_res);			
			break;
		case 0x02:
						netconn_connect(conn, addr, port);
						netconn_send(conn,buf_res);
			break;
		
		default:
			break;
	}
}
#endif /* LWIP_NETCONN */
